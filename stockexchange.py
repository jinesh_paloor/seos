import re
from collections import defaultdict


class StockOrder(object):
    """
    A stock order is an order to buy/sell a given quantity of stocks
    of specified company.
    """
    def __init__(self, side, company, quantity):
        self.company = company
        self.side = side
        self.quantity = quantity
        self.remaining_qty = quantity
        self.status = 'Open'

    def __str__(self):
        return '{0} - {1} - {2} - {3}'.format(
            self.company, self.side, self.quantity, self.status)

    def set_status(self):
        """
        Set status of a stock order based on the remaining quantity
        """
        self.status = 'Closed' if self.remaining_qty == 0 else 'Open'

    def set_quantity(self, open_stock):
        """
        Reset the remaining_quantity of stock order.
        the function expects to receive open_stock as parameter
        """
        if open_stock.remaining_qty >= self.remaining_qty:
            open_stock.remaining_qty -= self.remaining_qty
            self.remaining_qty = 0
        else:
            self.remaining_qty -= open_stock.remaining_qty
            open_stock.remaining_qty = 0

        self.set_status()
        open_stock.set_status()
        return True


class StockExchange(object):
    """
    Keep the track of stock orders.
    sales and buy happens through stock exchange.
    """
    def __init__(self):
        # keep the company wise open stock orders
        # format: {'company_name1': {'buy': [], 'sell':[]}, ...}
        self.open_stock_orders = defaultdict(dict)

        # keep the list of stock orders in the entered order
        # similar to a queue data structure
        self.stock_order_list = []

    def add_to_open_stock_order_list(self, stock_order):
        """
        Add a stock order in to open_stock_order_list.
        """
        # if stock order is not open dont add it.
        if stock_order.status != 'Open':
            return False

        # if open stock orders does not exist for the customer, create one
        if not self.open_stock_orders.get(stock_order.company):
            self.open_stock_orders[stock_order.company]['sell'] = []
            self.open_stock_orders[stock_order.company]['buy'] = []

        if stock_order.side == 'Sell':
            self.open_stock_orders[stock_order.company]['sell'].append(
                stock_order)
        else:
            self.open_stock_orders[stock_order.company]['buy'].append(
                stock_order)
        return True

    def remove_from_open_stock_order_list(self, stock_order):
        """
        Remove a stock order from open_stock_order_list
        """
        # if stock order is not close dont remove it.
        if stock_order.status != 'Closed':
            return False
        if stock_order.side == 'Sell':
            self.open_stock_orders[stock_order.company]['sell'].remove(
                stock_order)
        else:
            self.open_stock_orders[stock_order.company]['buy'].remove(
                stock_order)
        return True

    def order(self, stock_order):
        """
        Register a order and checks for open stocks to see whether requested
        stock operation can be performed.
        """
        customer_open_stocks = self.open_stock_orders.get(stock_order.company)
        # if open stock order not existing for the customer, dont do any
        # operations. return False
        opposite_side = 'sell' if stock_order.side == 'Buy' else 'buy'
        if not customer_open_stocks or not \
                customer_open_stocks.get(opposite_side):
            return False

        open_stocks = customer_open_stocks.get(opposite_side)
        for open_stock in open_stocks:
            stock_order.set_quantity(open_stock)
            if not stock_order.remaining_qty:
                break

        # remove open stocks wich is closed.
        for open_stock in open_stocks:
            if not open_stock.remaining_qty:
                self.remove_from_open_stock_order_list(open_stock)
        return True

    def add_to_stock_order_list(self, stock_order):
        """
        Add a stock order to stock_order_list. Before adding check whether any
        open_stock_order exists for the same company with opposite side.
        If exists, update the remaining_qty and status of both stock orders.
        """
        self.order(stock_order)
        # add to open stock order list.
        self.add_to_open_stock_order_list(stock_order)
        self.stock_order_list.append(stock_order)
        return True

    def show_stock_order_list(self):
        """
        Show the stock order list in terminal
        """
        print '_' * 75
        print 'Side\t\tCompany\t\tQuantity\tRemainingQty\tStatus'
        print '_' * 75
        for stock_order in self.stock_order_list:
            print '{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}'.format(
                stock_order.side, stock_order.company, stock_order.quantity,
                stock_order.remaining_qty, stock_order.status)
        print '_' * 75


def main():
    stock_exchange = StockExchange()
    # Enter side, quantity, and company. (multiple entry possible)
    while True:
        # get the input values: side, company , quantity respectively.
        side = raw_input("Enter side. Buy(b) / Sell(s) :")
        if not isinstance(side, str) or side not in ['s', 'b', 'S', 'B']:
            print 'Invalid side {0}'.format(side)
            continue

        company = raw_input("Enter Company.               :")
        if not re.match(r'[\w+\s+]', company):
            print 'Only Alphabets allowed for company name'
            continue

        quantity = raw_input("Enter Quantity               :")
        try:
            quantity = int(quantity)
        except ValueError:
            print 'Quantity {0} is not an integer value'.format(quantity)
            continue

        print '*' * 100
        side = 'Buy' if side in ['b', 'B'] else 'Sell'
        # Create a stock order with the input. The remaining quantity and
        # status will have default values.
        stock_order = StockOrder(side, company, quantity)

        # Add stock order into stock order list. Perform search in open stocks
        # to find opposite side orders for the same customer. if found update
        # the remaining_qty and status of stock order.
        stock_exchange.add_to_stock_order_list(stock_order)

        # show list of stock orders
        stock_exchange.show_stock_order_list()

        # Reapeat the process if there is more stock orders
        repeat = raw_input("Do you have more stock orders (y/n) :")
        if repeat in ['y', 'Y']:
            continue
        else:
            break

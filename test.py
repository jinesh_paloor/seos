import unittest
from stockexchange import StockOrder, StockExchange


class TestStockExchange(unittest.TestCase):

    def setUp(self):
        # data set for sample data given in requirement doc
        self.stock_exchange = StockExchange()
        self.stock_orders = [
                ('Buy', 'abc', 10), ('Sell', 'xyz', 15), ('Sell', 'abc', 13),
                ('Buy', 'xyz', 10), ('Buy', 'xyz', 8)]

        # sample data to test edge cases
        self.stock_exchange_1 = StockExchange()
        self.stock_orders_1 = [
                ('Buy', 'abc', 60), ('Sell', 'abc', 50),
                ('Buy', 'abc', 40), ('Sell', 'abc', 50)]

        # data set for load testing
        self.bulk_stock_exchange = StockExchange()
        self.bulk_stock_orders = [
                ('Buy', 'abc', 10), ('Sell', 'xyz', 15), ('Sell', 'abc', 13),
                ('Buy', 'xyz', 10), ('Buy', 'xyz', 8)] * 100000

    def test_stock_order(self):
        for side, company, qty in self.stock_orders:
            stock_order = StockOrder(side, company, qty)
            self.stock_exchange.add_to_stock_order_list(stock_order)

        self.stock_exchange.show_stock_order_list()
        # check total number of stock orders
        self.assertEqual(len(self.stock_exchange.stock_order_list), 5)
        # check number of open stock orders for 'abc' company
        self.assertEqual(len(self.stock_exchange.open_stock_orders.get('abc').get('sell')), 1)
        # check number of open stock orders for 'xyz' company
        self.assertEqual(len(self.stock_exchange.open_stock_orders.get('xyz').get('buy')), 1)

        # check the first stock order status
        self.assertEqual(self.stock_exchange.stock_order_list[0].remaining_qty, 0)
        self.assertEqual(self.stock_exchange.stock_order_list[0].company, 'abc')
        self.assertEqual(self.stock_exchange.stock_order_list[0].status, 'Closed')

        # check the fifth stock order status
        self.assertEqual(self.stock_exchange.stock_order_list[4].remaining_qty, 3)
        self.assertEqual(self.stock_exchange.stock_order_list[4].company, 'xyz')
        self.assertEqual(self.stock_exchange.stock_order_list[4].status, 'Open')

    def test_stock_order1(self):
        """
        To test the edge cases
        """
        for side, company, qty in self.stock_orders_1:
            stock_order = StockOrder(side, company, qty)
            self.stock_exchange_1.add_to_stock_order_list(stock_order)

        self.stock_exchange_1.show_stock_order_list()
        for stock in self.stock_exchange_1.stock_order_list:
            self.assertEqual(stock.status, 'Closed')

    def test_load(self):
        """
        Test with 5 lakh stock orders to test scalability.
        """

        for side, company, qty in self.bulk_stock_orders:
            stock_order = StockOrder(side, company, qty)
            self.bulk_stock_exchange.add_to_stock_order_list(stock_order)
            print '.',

        self.bulk_stock_exchange.show_stock_order_list()

        self.assertEqual(len(self.bulk_stock_exchange.stock_order_list), 500000)


if __name__ == '__main__':
    unittest.main()
